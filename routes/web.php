<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('tasks.index');
});

Route::get('tasks', 'App\Http\Controllers\TaskController@index')->name('tasks.index');
Route::post('tasks', 'App\Http\Controllers\TaskController@store')->name('tasks.store');
Route::put('tasks/sync', 'App\Http\Controllers\TaskController@sync')->name('tasks.sync');
Route::put('tasks/{task}', 'App\Http\Controllers\TaskController@update')->name('tasks.update');

Route::post('statuses', 'App\Http\Controllers\StatusController@store')->name('statuses.store');
Route::put('statuses', 'App\Http\Controllers\StatusController@update')->name('statuses.update');

