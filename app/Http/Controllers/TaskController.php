<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tasks = Task::find(1);
            $toDoList = ($tasks) ? $toDoList = $tasks->description : '';
            return response()->json(['status' => 'Ajax request', 'tasks' => $toDoList]);
        }

        return view('tasks.index');
    }


    public
    function store(Request $request)
    {
        $this->validate($request, [
            'todoTask' => ['required', 'string']
        ]);

        Task::updateOrCreate(['id' => 1], ['description' => $request->todoTask]);
        return response()->json(['success' => 'New Tasks Updated!']);
    }

    public
    function getTasks(Request $request)
    {
        $this->validate($request, [
            'todoTask' => ['required', 'string']
        ]);

        Task::updateOrCreate(['id' => 1], ['description' => $request->todoTask]);
        return response()->json(['success' => 'New Tasks Updated!']);
    }
}
