<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    private $_table = 'tasks';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_table))
            Schema::create($this->_table, function (Blueprint $table) {
                $table->id();
                $table->string('title')->nullable();
                $table->json('description')->nullable();
                $table->smallInteger('order')->default(0);
                $table->unsignedInteger('user_id')->nullable();
                $table->unsignedInteger('status_id')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_table);
    }
}
