<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTable extends Migration
{
    private $_table = 'statuses';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_table))
            Schema::create('statuses', function (Blueprint $table) {
                $table->id();
                $table->string('title');
                $table->string('slug');
                $table->smallInteger('order')->default(0);
                $table->unsignedInteger('user_id');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_table);
    }
}
