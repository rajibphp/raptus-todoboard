@extends('layouts.app')

<div id="loadingScreen">
    <div class="loader"></div>
</div>
<div class="controls p-3">


{{ Form::open(array('route' => 'tasks.store', 'method'=>'post','class'=>'form-inline addTaskForm')) }}
<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <input type="hidden" class="getTaskRoute" value="{{ route('tasks.index') }}">
                    <label for="titleInput">Title:</label>
                    <input class="form-control form-control-sm" type="text" name="title" id="titleInput"
                           autocomplete="off">
                    <label for="descriptionInput">Description:</label>
                    <input class="form-control form-control-sm" type="text" name="description" id="descriptionInput"
                           autocomplete="off">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" id="add">Add</button>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}


    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add New Task
    </button>
    <button class="btn btn-danger mx-2" id="deleteAll">Delete All</button>
</div>

<div class="boards overflow-auto p-0" id="boardsContainer">
</div>

<style>
    div.board:nth-child(1) > h3 {
        color: #EFA20C;
    }

    div.board:nth-child(2) > h3 {
        color: #158CCF;
    }

    div.board:nth-child(3) > h3 {
        color: #E2163B;
    }

</style>

@extends('layouts.footer')
