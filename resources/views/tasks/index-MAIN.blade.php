@extends('layouts.app')

<div id="loadingScreen">
    <div class="loader"></div>
</div>
<div class="controls p-3">

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>













    {{ Form::open(array('route' => 'tasks.store', 'method'=>'post','class'=>'form-inline addTaskForm')) }}
    <input type="hidden" class="getTaskRoute" value="{{ route('tasks.index') }}">
    <label for="titleInput">Title:</label>
    <input class="form-control form-control-sm" type="text" name="title" id="titleInput" autocomplete="off">
    <label for="descriptionInput">Description:</label>
    <input class="form-control form-control-sm" type="text" name="description" id="descriptionInput"
           autocomplete="off">
    <button class="btn btn-dark" id="add">Add</button>
    <button class="btn btn-danger mx-2" id="deleteAll">Delete All</button>
    {{ Form::close() }}
</div>
<div class="boards overflow-auto p-0" id="boardsContainer">
</div>

@extends('layouts.footer')
